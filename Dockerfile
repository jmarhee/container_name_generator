FROM golang:latest 
RUN mkdir /app 
ADD . /app/ 
WORKDIR /app  
RUN go get -v "github.com/docker/docker/pkg/namesgenerator"
RUN go build -o main . 
CMD ["/app/main"]
