package main

import (
	"fmt"
	"net/http"
	"encoding/json"
	"github.com/docker/docker/pkg/namesgenerator"
)

type Name struct {
	Name string `json:"name"`
}

func response(w http.ResponseWriter, r *http.Request) {
	name := string(namesgenerator.GetRandomName(0))
	body := Name{name}
	resp, err := json.Marshal(body)
	if err != nil {
		fmt.Println(err)
	}
	w.Write([]byte(resp))
}

func main() {
	http.HandleFunc("/", response)
	http.ListenAndServe(":8080", nil)
}
