package main

import (
	"testing"
	"net/http"
	"log"
	"os"
)

func TestConnect(t *testing.T) {
	resp, err := http.Get("http://localhost:8080")
	if err != nil {
		log.Println(err)
	}
	if int(resp.StatusCode) == 200 {
		log.Println(resp)
	} else {
		os.Exit(1)
	}
}

