Container Name Generator
=

[![buddy pipeline](https://app.buddy.works/interiorae/container-name-generator/pipelines/pipeline/197473/badge.svg?token=3a56b0b22fc414014ed4b6ad945504d22e0e1387f3466056d66f0db4755b0400 "buddy pipeline")](https://app.buddy.works/interiorae/container-name-generator/pipelines/pipeline/197473)

This project is a wrapper for the Docker `namegenerator` package to provide names over a web service.

